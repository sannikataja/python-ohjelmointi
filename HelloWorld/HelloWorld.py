greeting = "Hello World!"
print(greeting)
print('"How are you?"')

# risuaidalla kommentoidaan!
# print('\"Kenoviiva, jonka jälkeen tulee hipsu, printtaa lainausmerkit.\"')

# sum = greeting + str(10)
# print(sum)

# name = input("What is your name? >")
# name = name.capitalize()

name = input("What is your name? >")
# Pidempi tapa suurentaa kirjaimia alla.
# Onko name-muuttujassa vähintään 1 kirjain ja 
# onko ensimmäinen kirjain kirjoitettu pienellä
if len(name) > 0 and name[0].islower:
    # Koodiblocki merkataan sisennyksellä.
    # Esim. if-lauseen sisältö pitää kirjoittaa sisennettynä
# name[1:] tarkoittaa 1 eteenpäin. Kommentin ei tarvitse olla sisennetty.
    originalName = name
    name = name[0].upper()
    name += originalName[1:]
    # name = name + originalName[1:]
print(name)

longText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. \
 Praesent non accumsan libero. Ut auctor id arcu vitae dignissim. \
 Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; \
 Sed fringilla ex in porttitor vestibulum. Pellentesque imperdiet nisi ac elit dignissim, id tempus libero volutpat"
 # Jos on pitkä teksti, laita \ perään niin saa jaettua koodirivin useammalle riville.

