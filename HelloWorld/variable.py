# Pythonissa muuttujille ei tarvitse määrittää tyyppiä,
# muuttujilla silti on tyyppi, alla integer

int1 = 2
int2 = 5

result = 0

result += int1 + int2
print(result)

result -= int1 - int2
print(result)

result *= int1 * int2
print(result)

# Palauttaa jakolaskun floating pint arvona eli desimaalina
result /= int1 / int2
print(result)

# Jos kaksi //, palauttaa jakolaskun myös integerinä.
result = int1 // int2
print(result)

float1 = 2.5
float2 = 1.5

sum = float1 + float2
print(sum)

division = float1 / float2
print(division)

bool1 = True
bool2 = False

if bool1:
    print("Tosi")

int0 = 0
if int0 == 1:
    print("Tosi")
else:
    print("Epätosi")

# && -> and, || -> or